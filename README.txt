Hello Everyone.

This GIT is now available for the public!
It was used for the development of an autonomous robot used for a competition at the University of British Columbia.

The competition details are shown here:
http://projectlab.engphys.ubc.ca/enph253/competition-2013/

Our robot, finally named "Breaking Balls", turned out to be a beautiful and complex machine that can pick up objects (squash balls), navigate on a surface autonomously, and aim and shoot at different 'targets'. 

A short video of our robot's features can be found here:
http://youtu.be/OiqwCogbPqM

With merely 6 weeks to design and develop from scratch, the challenge of creating a fully autonomous robot was immense for the groups of second-year engineering physics students at UBC. Still, the lessons learned from this intense experience have already proved extremely valuable, and the competition itself would have planted inspirations for the aspiring engineers in the audience.

We are very glad to have been part of this amazing experience, and hope there will be a similar course in the future where we get to tackle the same challenge, but with a completely different set of knowledge and experience. :)

Everything on this repository are provided 'as-is', and there are no warranties or guarantees associated with any of the material. It has been released to the public under the GNU public license as an 'open-source' project. Feel free to be inspired by any of our contents, but do cite when sharing and/or using them. 

Here are a few things to remember:

1. Before you start editing files, always update your local repository!
2. Get in the habit of leaving USEFUL, EFFECTIVE comments in both the source code and GIT.