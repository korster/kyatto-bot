#include "ir.h"
#include "utilities.h"
#include "encoder.h"
#include "motorcontrol.h"

void TestIR()
{
  delay(1000);
  int leftIR = analogRead(LEFT_IR);
  int rightIR = analogRead(RIGHT_IR);
  while (!(Utilities::StopButton()))
  {  
    delay(100);
    LCD.clear();
    LCD.setCursor(0, 0); 
    LCD.print(leftIR);
    LCD.setCursor(0, 1); 
    LCD.print(rightIR); 
    //    Utilities::DisplayMenu(0,"Left",String(leftIR));
    //    Utilities::DisplayMenu(1,"Right", String(rightIR));
    leftIR = analogRead(LEFT_IR);
    rightIR = analogRead(RIGHT_IR);
  }
}

int SearchTarget()
/*
Description: SearchTarget function takes in the current wheel speed (which contains information of direction)
 then sweeps across the field, searching for IR lights below the targets facing the robot.
 
 Parameters: currSpeed = current speed of the robot (speed for both wheels)
 
 Return Values:
 0  =  No target found this way. Call this function again.
 1  =  Target found, and the robot is facing the centre of the target. Proceed to shoot.
 */
{
  // Move robot at currSpeed, constantly read IR. 
  // If LEFT_IR and RIGHT_IR are above 900, STOP the motors.
  // Turn wheels 90 degrees for correction.
  //   FOR CORRECTION: If currSpeed is +, pass in + angles for both wheels. If -, pass in -angles for both wheels.
  // If both IR detectors have value greater than 400 and are reading similar values (within +/- 100), then it must be looking at the target. Shoot.

  wheelSpeed = scanSpeed;
  motor.speed(LEFT_WHEEL, wheelSpeed);
  motor.speed(RIGHT_WHEEL, slantCorrection*wheelSpeed);
  int leftIR = analogRead(LEFT_IR);
  int rightIR = analogRead(RIGHT_IR);
  boolean WallCollision = false;
  boolean TargetDetected = false;

  float perc = slantCorrection;

  LCD.clear();
  LCD.setCursor(0, 0); 
  LCD.print("No Target");

  while ((leftIR < 400 || rightIR < 400) || (abs(leftIR-rightIR)>100 ))
    // If both IR detectors have value greater than 400 and are reading similar values (within +/- 100), then it must be looking at the target. Shoot.
  {
    leftIR = analogRead(LEFT_IR);
    rightIR = analogRead(RIGHT_IR);
    
    motor.speed(LEFT_WHEEL, wheelSpeed);
    motor.speed(RIGHT_WHEEL,((int) (wheelSpeed * perc)));

    // If the robot collides with a wall, break out of the function.
    if (!( (int) digitalRead(FRONT_LEFT_SWITCH) ) || !((int)digitalRead(FRONT_RIGHT_SWITCH)) || !(int)digitalRead(REAR_LEFT_SWITCH) || !(int)digitalRead(REAR_RIGHT_SWITCH))
      AligntoWall();
    if (FrontCollision() || RearCollision())
    {
      WallCollision = true;
      if (FrontCollision())
        wheelSpeed = -1*abs(wheelSpeed);
      
      if (RearCollision())
        wheelSpeed = abs(wheelSpeed);
    }
    if (Utilities::StopButton()) {
      StopMotors();
      break;
    }
  }

  if ( Utilities::StopButton() ) {
    StopMotors();
    Utilities::Menu();
  }

  // TARGET DETECTED! or Wall Collision occurred
  StopMotors();

  if ((leftIR > 400 || rightIR > 400) && abs(leftIR-rightIR)<150)
  {
    TargetDetected = true;
    LCD.clear();
    LCD.setCursor(0, 0); 
    LCD.print("Target Found!");
  }

  if (!WallCollision)  // Target was detected, in the middle of playing field. Adjust.
  {
    //Correct for IR sensor error.
    //while (!TurnAngle2(90, 90));
    /*
    if (wheelSpeed > 0)
      TurnAngle(-90, -90);
    if (wheelSpeed < 0)
      TurnAngle(90, 90);
    */
    return 1;
  }
  if (WallCollision == true && TargetDetected == false)  // Target was not detected, the bot just hit the wall.
  {
    wheelSpeed = -wheelSpeed;
    return 0;
  }

  if (WallCollision == true && TargetDetected == true) // Target was detected at the wall. It should be centered at the target, so shoot away.
    return 1;
}

