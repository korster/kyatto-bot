#include <phys253.h>        //   ***** from 253 template file
#include <LiquidCrystal.h>  //   ***** from 253 template file

#include "common.h"
#include "encoder.h"
#include "utilities.h"
#include "tapefollowing.h"
#include "ir.h"
#include "motorcontrol.h"

void setup()
{
  portMode(0, INPUT) ;      //   ***** from 253 template file
  portMode(1, INPUT) ;      //   ***** from 253 template file
  Serial.begin(9600);

  RCServo0.attach(RCServo0Output) ;
  RCServo1.attach(RCServo1Output) ;
  RCServo2.attach(RCServo2Output) ;

  ServoGatesDefault();
  LoaderDefault();
  StopMotors();
}

int currentState = DEBUG;
int currTime = millis();
int started = 0;
int count = 0;

void loop()
{

  if (currentState == TAPE_FOLLOW_BACKWARDS) {
    if (wheelSpeed > 0) {
      wheelSpeed *= (-1);
    }
    while ((int)digitalRead(REAR_LEFT_SWITCH) && (int)digitalRead(REAR_RIGHT_SWITCH))
    {
      TapeFollow(wheelSpeed, currState);
      if ( Utilities::StopButton() ) {
        StopMotors();
        Utilities::Menu();
      }
    }
    wheelSpeed = (-1)*abs(wheelSpeed);
    StopMotors();
    currentState = PICK_UP;
  }

  if (currentState == ALIGN_TO_WALL) {
    // For testing, aligns to wall, and tries to pick up ballz.
    // Then it moves forward, turns 45 degrees to left, then aligns itself again. 
    // Repeats above step but turning to the right.
    wheelSpeed = -660;
    while (!stopbutton()) {
      AligntoWall();
      StopMotors();
      if (RearCollision()) {
        BallPickup(); 
        delay(3000);
        /* Following code is for picking up ball at the corners. Ask Jon if it is okay for us to align the balls at the centre during comp. */
        // Move forward then turn by 45 degrees to the LEFT.
        TurnAngle(180,180);
        TurnAngle(-90, 90);
        wheelSpeed = -660;
        AligntoWall();
        BallPickup();
        delay(1500);

        // Move forward then turn by 45 degrees to the RIGHT.
        TurnAngle(180, 180);
        TurnAngle(120, -120);
        wheelSpeed = -660;
        AligntoWall();
        BallPickup();
        //while (!BallReady());
        delay(1500);

        while (!startbutton()) {
          StopMotors();
          LCD.clear();
          LCD.home(); 
          LCD.print("READY 2 GO");
          delay(500);
        }
      }
    }
  }

  if (currentState == PICK_UP) {
    AligntoWall();
    BallPickup();
    currentState = BLIND_DRIVE_FWD;
  } 

  if (currentState == TAPE_FOLLOW_FORWARDS) {
    if (wheelSpeed < 0) {
      wheelSpeed *= -1;
    }
    TapeFollow(wheelSpeed, currState);
    if (wheelSpeed > 0) {
      if ( CheckForTapeT() ) {
        StopMotors();
        wheelSpeed = turnSpeed;
        TurnAngle(-180, -180);
        TurnAngle(-270,270);
        currentState = SCAN_FOR_IR;
      }
    }
  }

  if (currentState == BLIND_DRIVE_FWD) {
    wheelSpeed = upwardSpeed;
    motor.speed(LEFT_WHEEL, wheelSpeed);
    motor.speed(RIGHT_WHEEL, wheelSpeed); 
    while ((int)digitalRead(FRONT_LEFT_SWITCH) && (int)digitalRead(FRONT_RIGHT_SWITCH))
    {
      if (Utilities::StopButton()) {
        StopMotors();
        Utilities::Menu();
      }
    }
    AligntoWall();
    StopMotors();
    wheelSpeed = turnSpeed; 
    TurnAngle(-315, -315);
    StopMotors();
    TurnAngle(-315, 315); 
    StopMotors();

    currentState = SCAN_FOR_IR;
  }

  if (currentState == SCAN_FOR_IR) {
    // ACTUAL CODE FOR SCANNING AND SHOOTING
    while (BallReady()) // If there is no ball, don't bother searching for targets.
    {
      if (SearchTarget())  // If target found...
      {
        // Stop and Shoot
        StopMotors();
        LCD.clear(); 
        LCD.home();
        LCD.print("TGT FOUND!");
        // Overshoot was adjusted.

        // if ball is in the ramp/track, shoot it. (See definition of BallShooter() in "motorcontrol.cpp")
        //if (BallReady()) {
        BallShooter(shooterSpeed);
        //  ballCount++;
        //}
        //else {
        //  failCount++;
        //}
      }
      else  // All targets facing opponent. Wait and search again.
      {
        delay(1000);
      }
    }
    delay(1000);
    currentState = RETURN_TO_T;
  }

  if (currentState == RETURN_TO_T) {
    LCD.clear(); 
    LCD.home();
    LCD.print("NO MORE BALL");
    AligntoWall();
    StopMotors();
    int blackTapeCount = 0;
    int multiCountPrevention = 0;  //  For hysterisis.

    LCD.clear(); 
    LCD.home();
    LCD.print("RETURN TO TAPE");

    if (RearCollision())
    {
      // Go straight until you hit the second black tape on the MIDDLE_LEFT_QRD.
      wheelSpeed = scanSpeed;
      motor.speed(LEFT_WHEEL, wheelSpeed);
      motor.speed(RIGHT_WHEEL, slantCorrection*wheelSpeed);
      while (blackTapeCount < 2) {
        if (digitalRead(MIDDLE_LEFT_QRD) && multiCountPrevention == 0)
        {
          multiCountPrevention = 1;
          blackTapeCount++;
        }
        if (!digitalRead(MIDDLE_LEFT_QRD))
          multiCountPrevention = 0;
      }
      StopMotors();
      // Turn until the rear right QRD is on tape.
      while (!digitalRead(REAR_RIGHT_QRD))
        TurnAngle(encResolution, -encResolution);
      StopMotors();
    }

    // Go straight until you hit the secodn black tape on MIDDLE_LEFT_QRD.
    else if (FrontCollision())
    {
      wheelSpeed = -scanSpeed;
      motor.speed(LEFT_WHEEL, slantCorrection*wheelSpeed);
      motor.speed(RIGHT_WHEEL, wheelSpeed);

      while (blackTapeCount < 2) {
        if (digitalRead(MIDDLE_LEFT_QRD) && multiCountPrevention == 0)
        {
          multiCountPrevention = 1;
          blackTapeCount++;
        }
        if (!digitalRead(MIDDLE_LEFT_QRD))
          multiCountPrevention = 0;
      }
      StopMotors();
      // Turn until the rear right QRD is on tape.
      while (!digitalRead(REAR_RIGHT_QRD))
        TurnAngle(encResolution, -encResolution);
      StopMotors();
    }
    currentState = TAPE_FOLLOW_BACKWARDS;
}

if (currentState == FULL_RUN) {
  LCD.clear(); 
  LCD.home();
  LCD.print("Starting...");

  if (started == 0)                        // If running for the first time, wait for start button.
    while (!startbutton())
    {
      if (Utilities::StopButton())
        Utilities::Menu();
    }  // Don't start until start button is pressed.

  started = 1;

  currentState = TAPE_FOLLOW_BACKWARDS;

  /*
    wheelSpeed = -abs(downwardSpeed);
   currTime = millis();
   
   // TAPE FOLLOW BACKWARDS
   LCD.clear(); 
   LCD.home();
   LCD.print("TapeFlw BACK");
   while ((millis()-currTime) < firstBackingUpTime) {
   TapeFollow(wheelSpeed, currState);
   if ((digitalRead(FRONT_LEFT_QRD) && digitalRead(FRONT_RIGHT_QRD)) && (digitalRead(REAR_LEFT_QRD) && digitalRead(REAR_RIGHT_QRD)))
   {
   StopMotors();
   break;
   }
   if (Utilities::StopButton()) {
   StopMotors();
   break;
   }
   }
   
   // ALIGN TO WALL
   LCD.clear(); 
   LCD.home();
   LCD.print("ALGN TO WALL");
   wheelSpeed = -abs(downwardSpeed);
   AligntoWall();
   StopMotors();
   
   // PICK UP THE BALLS
   int pickUpStartTime = millis();
   int pickUpSpeed = default_pickUpSpeed;
   
   LCD.clear(); 
   LCD.home();
   LCD.print("PICK UP BALL");
   BallPickup();
   while (!BallReady())   // Wait until at least one ball enters ramp.
   {
   pickUpSpeed = default_pickUpSpeed + (int)((millis() - pickUpStartTime)/10.0);
   BallPickup(pickUpSpeed);
   if (Utilities::StopButton())
   {
   StopMotors();
   LCD.clear(); 
   LCD.home();
   LCD.print("PAUSED. <st>");
   while (!startbutton());
   };
   }
   delay(3000);
   StopMotors();    
   
   // FIND RIGHT TAPE TRACK
  /*
   LCD.clear(); 
   LCD.home();
   LCD.print("FIND R TAPE");
   TurnAngle(180, 180);
   TurnAngle(90, -90);
   wheelSpeed = abs(wheelSpeed);
   while(!digitalRead(FRONT_RIGHT_QRD) || !digitalRead(FRONT_LEFT_QRD)) {
   if (stopbutton())
   {
   StopMotors();
   LCD.clear(); 
   LCD.home();
   LCD.print("PAUSED. <st>");
   while (!startbutton());
   }
   TurnAngle(45, 45);
   }
   StopMotors();
   */


  /*

   // Move forward for a bit.
   wheelSpeed = upwardSpeed;
   TurnAngle(360, 360);
   
   // TAPE FOLLOW FORWARDS until T.
   LCD.clear(); 
   LCD.home();
   LCD.print("TAPE FLW FWD");
   wheelSpeed = abs(wheelSpeed);
   while(!((int) digitalRead(MIDDLE_LEFT_QRD) ) || !((int) digitalRead(MIDDLE_RIGHT_QRD)) ) {
   TapeFollow(wheelSpeed, currState);
   if (stopbutton())
   {
   StopMotors();
   LCD.clear(); 
   LCD.home();
   LCD.print("PAUSED. <st>");
   while (!startbutton());
   }
   }
   StopMotors();
   
   // Turn 90 degs to the LEFT.
   LCD.clear(); 
   LCD.home();
   LCD.print("TURN 90");
   TurnAngle(-360, 360);
   
   if (stopbutton())
   {
   StopMotors();
   LCD.clear(); 
   LCD.home();
   LCD.print("PAUSED. <st>");
   while (!startbutton());
   }
   
   while(BallReady()) {
   LCD.clear(); 
   LCD.home();
   LCD.print("SEARCH TGT");
   if (SearchTarget())  // If target found...
   {
   // Stop and Shoot
   StopMotors();
   LCD.clear(); 
   LCD.home();
   LCD.print("TGT FOUND!");
   // Overshoot was adjusted.
   
   BallShooter(shooterSpeed);
   }
   else  // All targets facing opponent. Wait and search again.
   {
   delay(1000);
   }
   }
   
   // After running out of the balls, return to middle T.
   LCD.clear(); 
   LCD.home();
   LCD.print("NO MORE BALL");
   AligntoWall();
   StopMotors();
   int blackTapeCount = 0;
   int multiCountPrevention = 0;  //  For hysterisis.
   
   LCD.clear(); 
   LCD.home();
   LCD.print("RETURN TO TAPE");
   
   if (RearCollision())
   {
   // Go straight until you hit the second black tape on the MIDDLE_LEFT_QRD.
   wheelSpeed = 660;
   motor.speed(LEFT_WHEEL, wheelSpeed);
   motor.speed(RIGHT_WHEEL, slantCorrection*wheelSpeed);
   while (blackTapeCount < 2) {
   if (digitalRead(MIDDLE_LEFT_QRD) && multiCountPrevention == 0)
   {
   multiCountPrevention = 1;
   blackTapeCount++;
   }
   if (!digitalRead(MIDDLE_LEFT_QRD))
   multiCountPrevention = 0;
   }
   StopMotors();
   // Turn until the rear right QRD is on tape.
   while (!digitalRead(REAR_RIGHT_QRD))
   TurnAngle(30, -30);
   StopMotors();
   }
   
   // Go straight until you hit the secodn black tape on MIDDLE_LEFT_QRD.
   else if (FrontCollision())
   {
   wheelSpeed = -660;
   motor.speed(LEFT_WHEEL, slantCorrection*wheelSpeed);
   motor.speed(RIGHT_WHEEL, wheelSpeed);
   
   while (blackTapeCount < 2) {
   if (digitalRead(MIDDLE_LEFT_QRD) && multiCountPrevention == 0)
   {
   multiCountPrevention = 1;
   blackTapeCount++;
   }
   if (!digitalRead(MIDDLE_LEFT_QRD))
   multiCountPrevention = 0;
   }
   StopMotors();
   // Turn until the rear right QRD is on tape.
   while (!digitalRead(REAR_RIGHT_QRD))
   TurnAngle(30, -30);
   StopMotors();
   }
   // Back at 'starting' position. Loop.
   LCD.clear(); 
   LCD.home();
   LCD.print("GO BCK 2 CHOPPA");
   
   */
}
///////////////////
///////DEBUG///////
///////////////////
if (currentState == DEBUG) {

  int currentDebug = (NUM_DEBUG/1024.0) * knob(6);
  static int c;
  c++;
  if (c==200) {
    LCD.home();
    LCD.print(BLANK);
    LCD.home();
    LCD.print(debug[currentDebug]);
    c=0;
  }

  if (currentDebug == TEST_QRDS) {
    StopMotors();
    static int c1;
    static int currentQRD = 0;
    c1++;
    if (c1==200) {
      LCD.setCursor(0,1);
      LCD.print(BLANK);
      LCD.setCursor(0,1);
      if (currentQRD == 0) {
        LCD.print("FL:"); 
        LCD.print((int) digitalRead(FRONT_LEFT_QRD));
        LCD.print("FR:"); 
        LCD.print((int) digitalRead(FRONT_RIGHT_QRD));
        LCD.print("EL:"); 
        LCD.print((int) digitalRead(ENCODER_LEFT_QRD));
        LCD.print("ER:"); 
        LCD.print((int) digitalRead(ENCODER_RIGHT_QRD));
      }
      if (currentQRD == 1) {
        LCD.print("ML:"); 
        LCD.print((int) digitalRead(MIDDLE_LEFT_QRD));
        LCD.print("MR:"); 
        LCD.print((int) digitalRead(MIDDLE_RIGHT_QRD));
        LCD.print("RL:"); 
        LCD.print((int) digitalRead(REAR_LEFT_QRD));
        LCD.print("RR:"); 
        LCD.print((int) digitalRead(REAR_RIGHT_QRD));
      }
      if (currentQRD == 2) {
        LCD.print("FLS");
        LCD.print((int) digitalRead(FRONT_LEFT_SWITCH)); 
        LCD.print("FRS");
        LCD.print((int) digitalRead(FRONT_RIGHT_SWITCH)); 
        LCD.print("RLS"); 
        LCD.print((int) digitalRead(REAR_LEFT_SWITCH));
        LCD.print("RRS"); 
        LCD.print((int) digitalRead(REAR_RIGHT_SWITCH));
      }
      c1=0;
    }

    if(startbutton() ) {
      delay(50);
      if (!startbutton() ) {
        currentQRD++;
      }
    }

    if (currentQRD == 3) {
      currentQRD = 0;
    }
  }

  if (currentDebug == TEST_MOTORS) {
    static int currentMotor = 0;
    static int c2 = 0;
    c2++;

    if(startbutton() ) {
      delay(50);
      if (!startbutton() ) {
        StopMotors();
        currentMotor++;
      }
    }

    if (currentMotor == NUM_MOTORS) {
      currentMotor = 0;
    }

    int motorSpeed = (2.0 * knob(7) - 1023.0);
    if (c2 == 200) {
      LCD.setCursor(0,1);
      LCD.print(BLANK);
      LCD.setCursor(0,1);
      LCD.print(motors[currentMotor]);
      LCD.print(": ");
      LCD.print(motorSpeed);
      c2=0;
    }
    motor.speed(currentMotor,motorSpeed);
  }

  if (currentDebug == TEST_SERVOS) {
    StopMotors();

    static int c3;
    static int currentServo;
    int angle;
    if (currentServo == 0) {
      angle = (115/1023.0 * knob(7) + 30);
      RCServo0.write(angle);
    }
    if (currentServo == 1) {
      angle = (90/1023.0 * knob(7));
      RCServo1.write(angle);
    }
    if (currentServo == 2) {
      angle = (90/1023.0 * knob(7));
      RCServo2.write(angle);
    }
    if (startbutton()) {
      delay(50);
      if (!(startbutton()) ) {
        currentServo++;
      }
    }

    if (currentServo == 3) {
      currentServo = 0;
    }

    if (c3 == 200) {
      LCD.setCursor(0,1);
      LCD.print(BLANK);
      LCD.setCursor(0,1);
      LCD.print("Servo ");
      LCD.print(currentServo);
      LCD.print(": ");
      LCD.print(angle);
      c3=0;
    }
    c3++;
  }

  if (currentDebug == TEST_TURNING) {
    static int L; 
    static int R;
    static int nL;
    static int nR;
    static int current = 0;
    static int c4;

    LCD.setCursor(0,1);
    LCD.print("<start> to turn");

    if (startbutton() ) {
      delay(100);
      if (!(startbutton() ) ) {
        current++;
      }
    }

    while ((current <= 3) && current > 0) {
      if (current == 1) {
        nL = 41/1024.0*knob(7)-20;
        nL *= 30;
        if (stopbutton() ) {
          L = nL;
        }
      }
      if (current == 2) {
        nR = 41/1024.0*knob(7)-20;
        nR *=45;
        if (stopbutton() ) {
          R = nR;
        }
      }
      if (current == 3) {
        if (stopbutton() ) {
          current = 1;
        }
      }

      if (startbutton() ) {
        delay(50);
        if (!(startbutton() ) ) {
          current++;
        }
      }

      if (c4 == 200) {
        LCD.home();
        LCD.clear();
        LCD.print("TEST_TURN");
        LCD.setCursor(0,1);
        LCD.print(BLANK);
        LCD.setCursor(0,1);
        if (current == 1) {
          LCD.print("L: ");
          LCD.print(L);
          LCD.print(" !L: ");
          LCD.print(nL);
        } 
        else if (current == 2) {
          LCD.print("R: ");
          LCD.print(R);
          LCD.print(" !R: ");
          LCD.print(nR);
        }
        else if (current == 3) {
          LCD.print("L: ");
          LCD.print(L);
          LCD.print(" R: ");
        }
        LCD.print(R);
        c4=0;
      }
      c4++;
    }
    if (current == 4) {
      current = 0;
      TurnAngle(L,R);
    }
  }

  if (currentDebug == TEST_IR) {
    static int c5;
    if (startbutton() ) {
      TestIR();
    }

    if (c5==200) {
      LCD.setCursor(0,1);
      LCD.print(BLANK);
      c5=0;
    }     
    c5++;
  }
}

if ( Utilities::StopButton() ) {
  StopMotors();
  Utilities::Menu();
}
}
