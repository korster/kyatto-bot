#ifndef ENCODER_H
#define ENCODER_H

#include "common.h"

void TurnAngle(int desiredAngleL, int desiredAngleR);
//Turns each wheel specific angle (must be multiple of 45)

void Turn90atT();
//Rotates the robot 90 degrees at the T

void TurnAngleAtT(int desiredAngleL, int desiredAngleR);
//Turns each wheel specific angle (must be multiple of 45) until the middle left QRD touches the tape

#endif
