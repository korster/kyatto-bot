#ifndef TAPEFOLLOWING
#define TAPEFOLLOWING

#include "common.h"

int PIDError(const int leftQRDPort, const int rightQRDPort, int lastError);
//Calculates PID error based on state of each QRD and previous error

int PIDErrorForwards(int lastError);
//calculates PID error based on state of QRD and previous error

void TapeFollow(int wheelSpeed, int currState);
//Follows tape, must repeatedly call

boolean CheckForTapeT(void);
//Checks if robot is at tape T's

boolean FrontCollision(void);
//Checks if front of robot hit wall

boolean RearCollision(void);
//Checks if back of robot hit wall

void SteerLeft();
//Steer left after ball pick-up to follow left tape

#endif
