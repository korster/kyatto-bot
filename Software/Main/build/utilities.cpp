
#include "utilities.h"

namespace Utilities {

  //LCD
  String CheckStringLength( String s, int _length) {
    //if string is too short, append spaces
    while (s.length() < _length) {
      s = s.concat(" ");
    }
    //if string is too long, take substring
    if ( s.length() > _length) {
      s=s.substring(0, _length);
      return s;
    }
    else {
      return s;
    }
  }

  boolean StopButton() {
    if (stopbutton() ) {
      delay(50);
      if (stopbutton() ) {
        return true;
      }
    }
    return false;
  }
  
  void Menu() {
    //follow format provided, ignore the states menu, which is custom.
    delay(500);
    static int c;
    while (! (startbutton())) {
      
      int newValue1 = knob(6);
      int newValue2 = knob(7);
      
      if (c==200) {
        LCD.clear();
        LCD.home();
        LCD.print("kp: ");
        LCD.print(kp);
        LCD.print(" kd: ");
        LCD.print(kd);
        LCD.setCursor(0,1);
        LCD.print("kp: ");
        LCD.print(newValue1);
        LCD.print(" kd: ");
        LCD.print(newValue2);
        c=0;
      }
      c++;
      if (stopbutton() ) {
        kp = newValue1;
        kd = newValue2;
      }
    }
    
    LCD.clear();
    delay(300);

    while(! (startbutton() )) {
      
      int newValue1 = 2*knob(6) - 1023.0;
      
      if (c==200) {
        LCD.clear();
        LCD.home();
        LCD.print("Speed: ");
        LCD.print(wheelSpeed);
        LCD.setCursor(0,1);
        LCD.print("Speed: ");
        LCD.print(newValue1);
        c=0;
      }
      c++;
      
      if (stopbutton() ) {
        wheelSpeed = newValue1;
      }
    }
    delay(300);

    while(! (startbutton() )) {
      int newValue = ((int) ( (NUM_STATES-1)/1023.0 * knob(6) ));
      if ( stopbutton() ) {
        currentState = newValue;
      }
      if (c==200) {
        LCD.clear();
        LCD.home();
        LCD.print(states[currentState]);
        LCD.setCursor(0,1);
        LCD.print(states[newValue]);
        c=0;
      }
      c++;;
    }

    LCD.clear();
  }
}

