#include "encoder.h"
#include "utilities.h"
#include "motorcontrol.h"

  int encResolution = 45;
  
void TurnAngle( int desiredAngleL, int desiredAngleR) {

  int speedLeft = abs(wheelSpeed);
  int speedRight = abs(wheelSpeed);

  if (desiredAngleL < 0) {
    speedLeft *= -1;
  }

  if (desiredAngleR < 0) {
    speedRight *= -1;
  }

  int encoderLeftCurr = digitalRead(ENCODER_LEFT_QRD);
  int encoderRightCurr = digitalRead(ENCODER_RIGHT_QRD);
  int encoderLeftPrev = encoderLeftCurr;
  int encoderRightPrev = encoderRightCurr;
  int encDebounceTime = 50;
  int encResolution = 45;
  int c = 0;

  int speedDropOffMax = 60;      // used for slowing down robot as it turns.
  if (wheelSpeed >= 600)
    speedDropOffMax = wheelSpeed - 600;

  if (wheelSpeed < 600)
    speedDropOffMax = 0;

  long int errorCountStartTime = millis();
  long int errorCountTime = 1000; // Miliseconds.

  int errorLeft = 0; // # of state changes overshot.
  int errorRight = 0;

  //boolean errorFixedLeft = false;
  //boolean errorFixedRight = false;

  while ((angleR != desiredAngleR) || (angleL != desiredAngleL) ) {
    if (c == 200)
    {
      LCD.clear();
      LCD.setCursor(0,0); 
      LCD.print(encoderLeftCurr);
      LCD.setCursor(2,0); 
      LCD.print(angleL);
      LCD.setCursor(0,1); 
      LCD.print(encoderRightCurr);
      LCD.setCursor(2,1); 
      LCD.print(angleR);   
      c = 0;
    }
    c++;

    encoderLeftCurr = digitalRead(ENCODER_LEFT_QRD);
    encoderRightCurr = digitalRead(ENCODER_RIGHT_QRD);

    //while either angle is not equal to desired
    if (angleR != desiredAngleR) {
      //specifically for right wheel
      if (encoderRightCurr != encoderRightPrev) {
        delay(encDebounceTime);
        if (encoderRightCurr != encoderRightPrev) {
          if (desiredAngleR < 0) {
            angleR -= encResolution;
          } 
          else {
            angleR += encResolution;
          }
          /*
          // Drops speed as it gets closer to desiredAngle.
          if (abs((float)angleR/desiredAngleR) >= 0.5) // Start dropping the speed after angleR/desiredAngleR >= 0.5)
          {
            if (desiredAngleR > 0)
              speedRight = speedRight - abs(speedDropOffMax*((float)angleR/desiredAngleR));
            if (desiredAngleR < 0)
              speedRight = speedRight + abs(speedDropOffMax*((float)angleR/desiredAngleR));
          }
          */
          encoderRightPrev = encoderRightCurr;
        }
      }
    }
    else {
      speedRight = 0;
      motor.speed(RIGHT_WHEEL, speedRight);
    }

    if (angleL != desiredAngleL) {
      //specifically for left wheel
      if (encoderLeftCurr != encoderLeftPrev) {
        delay(encDebounceTime);
        if (encoderLeftCurr != encoderLeftPrev) {
          if (desiredAngleL < 0) {
            angleL -= encResolution;
          } 
          else {
            angleL += encResolution;
          }
          if (abs((float)angleL/desiredAngleL) >= 0.5) {
            if (desiredAngleL > 0)
              speedLeft = speedLeft - abs(speedDropOffMax*((float)angleL/desiredAngleL));
            if (desiredAngleL < 0)
              speedLeft = speedLeft + abs(speedDropOffMax*((float)angleL/desiredAngleL));
          }
          encoderLeftPrev = encoderLeftCurr;
        }
      }
    }
    else {
      speedLeft = 0;
      motor.speed(LEFT_WHEEL, speedLeft);
    }

    motor.speed(LEFT_WHEEL, speedLeft);
    motor.speed(RIGHT_WHEEL, speedRight);

    if ( stopbutton() ) {
      break;
    }
  }


  LCD.clear();
  LCD.setCursor(0,0); 
  LCD.print("FINISHED TURN");

  //////////////////////////
  ///////CORRECTION/////////
  //////////////////////////
  // Continue to read state changes for 1 second. Increase error counter for each wheel
  // Go the other way by # of counts at 90 percent of original speed.

  errorCountStartTime = millis();

  // Count drift.
  //  if( ((angleR == desiredAngleR) && (angleL == desiredAngleL))) {
  //    LCD.clear();
  //    LCD.setCursor(0,0); 
  //    LCD.print("Countin Drift");
  //    while ((millis()-errorCountStartTime) < errorCountTime) {
  //      encoderLeftCurr = digitalRead(ENCODER_LEFT_QRD);
  //      encoderRightCurr = digitalRead(ENCODER_RIGHT_QRD);
  //      if (encoderLeftCurr != encoderLeftPrev)
  //      {
  //        errorLeft++;
  //        encoderLeftPrev = encoderLeftCurr;
  //      }
  //
  //      if (encoderRightCurr != encoderRightPrev)
  //      {
  //        errorRight++;
  //        encoderRightPrev = encoderRightCurr;
  //      }
  //      LCD.setCursor(0,1); 
  //      LCD.print(errorLeft);
  //      LCD.setCursor(8,1); 
  //      LCD.print(errorRight);
  //    }
  //  }
  //
  //  while (errorLeft > 0 || errorRight > 0) {
  //    if (errorLeft > 0) {
  //      motor.speed(LEFT_WHEEL, -speedLeft*0.85);
  //    }
  //    if (errorRight > 0) {
  //      motor.speed(RIGHT_WHEEL, -speedRight*0.85);
  //    }
  //
  //    encoderLeftCurr = digitalRead(ENCODER_LEFT_QRD);
  //    encoderRightCurr = digitalRead(ENCODER_RIGHT_QRD);
  //
  //    if (encoderLeftCurr != encoderLeftPrev)
  //    {
  //      errorLeft--;
  //      encoderLeftPrev = encoderLeftCurr;
  //      LCD.setCursor(0,1); 
  //      LCD.print(errorLeft);
  //    }
  //
  //    if (encoderRightCurr != encoderRightPrev)
  //    {
  //      errorRight--;
  //      encoderRightPrev = encoderRightCurr;
  //      LCD.setCursor(8,1); 
  //      LCD.print(errorRight);
  //    }
  //
  //    if (errorLeft == 0)
  //    {
  //      motor.speed(LEFT_WHEEL, 0);
  //    }
  //    if (errorRight == 0)
  //    {
  //      motor.speed(RIGHT_WHEEL, 0);
  //    }
  //    if ( stopbutton() ) {
  //      Utilities::Menu();
  //    }
  //  }

  //  
  //  if ( ( abs(angleL) >= abs(desiredAngleL)) && (abs(angleR) >= abs(desiredAngleR)) ) {
  //    while ((encoderRightCurr != encoderRightPrev) || (encoderLeftCurr != encoderLeftPrev))
  //    {
  //      encoderRightCurr = digitalRead(ENCODER_RIGHT_QRD);
  //      encoderLeftCurr = digitalRead(ENCODER_LEFT_QRD);
  //      // Turn slightly back to cancel out the error.
  //      if (encoderLeftCurr != encoderLeftPrev) {
  //        motor.speed(LEFT_WHEEL, -(speedLeft));
  //      }
  //      if (encoderRightCurr != encoderRightPrev) {
  //        motor.speed(RIGHT_WHEEL, -(speedRight));
  //      }
  //
  //      if (stopbutton() ) {
  //        LCD.clear();
  //        break;
  //      }
  //    }
  //  } 

  StopMotors();
  angleL = 0;
  angleR = 0;
  if (Utilities::StopButton() ) {
    Utilities::Menu();
  }
}

void Turn90atT(void)
//Rotates the robot 90 degrees at the T
{
  int angleMax = 360; // double check this value
  int angleIncrement = 45; // this value might be too slow or small
  int count = 0;
  
  while (count < angleMax/angleIncrement) 
  {
    TurnAngle(-angleIncrement,angleIncrement);
    count++;
    if (!digitalRead(MIDDLE_LEFT_QRD) && (count > 2) ) 
    // This can be a problem if the tape is skipped during TurnAngle
    // Skip the first couple ML_QRD check to get off the T first
    {
      StopMotors();
      break;
    }
    if (Utilities::StopButton())
    {
      break;
    }
  }
}

void TurnAngleAtT( int desiredAngleL, int desiredAngleR) {

  // possibly offset the wheel speeds

  int speedLeft = abs(wheelSpeed);
  int speedRight = abs(wheelSpeed);

  if (desiredAngleL < 0) {
    speedLeft *= -1;
  }
  if (desiredAngleR < 0) {
    speedRight *= -1;
  }

  int encoderLeftCurr = digitalRead(ENCODER_LEFT_QRD);
  int encoderRightCurr = digitalRead(ENCODER_RIGHT_QRD);
  int encoderLeftPrev = encoderLeftCurr;
  int encoderRightPrev = encoderRightCurr;
  int encDebounceTime = 50;
  int encResolution = 45;
  int c = 0;

  long int encoderRightPrevTime = millis();
  long int encoderLeftPrevTime = millis();

  while ((angleR != desiredAngleR) || (angleL != desiredAngleL) ) {

    if (c == 200)
    {
      LCD.clear();
      LCD.setCursor(0,0); 
      LCD.print(encoderLeftCurr);
      LCD.setCursor(2,0); 
      LCD.print(angleL);
      LCD.setCursor(0,1); 
      LCD.print(encoderRightCurr);
      LCD.setCursor(2,1); 
      LCD.print(angleR);   
      c = 0;
    }
    c++;

    encoderLeftCurr = digitalRead(ENCODER_LEFT_QRD);
    encoderRightCurr = digitalRead(ENCODER_RIGHT_QRD);

    //while either angle is not equal to desired
    if (angleR != desiredAngleR) {
      //specifically for right wheel
      if ( (encoderRightCurr != encoderRightPrev) && (millis()-encoderRightPrevTime > encDebounceTime) ) { // compare times instead of delay
        encoderRightPrevTime = millis();

        if (desiredAngleR < 0) {
          angleR -= encResolution;
        } else {
          angleR += encResolution;
        }

        encoderRightPrev = encoderRightCurr;
      }
    }
    else {
      speedRight = 0;
    }

    if (angleL != desiredAngleL) {
      //specifically for left wheel
      if ( (encoderLeftCurr != encoderLeftPrev) && (millis()-encoderLeftPrevTime > encDebounceTime) ) {
        encoderLeftPrevTime = millis();

        if (desiredAngleL < 0) {
          angleL -= encResolution;
        } else {
          angleL += encResolution;
        }

        encoderLeftPrev = encoderLeftCurr;
      }
    }
    else {
      speedLeft = 0;
    }

    motor.speed(LEFT_WHEEL, speedLeft);
    motor.speed(RIGHT_WHEEL, speedRight);

    if ( stopbutton() ) {
      break;
    }
  }


  LCD.clear();
  LCD.setCursor(0,0); 
  LCD.print("FINISHED TURN");

  StopMotors();
  angleL = 0;
  angleR = 0;
}
