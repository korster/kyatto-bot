#ifndef MOTOR_H
#define MOTOR_H

#include "common.h"
#include "tapefollowing.h" //for CurrentTapeTrack

void StopMotors(void);
//stops all motors

int LimitMotorSpeed(int motorSpeed);
//limits motor speed to +/- 1023

int MinMotorSpeed(int motorSpeed);
// limits motor speed to +/- 'minimumSpeed'

boolean BallReady(void);
// Checks if ball is loaded in the ramp.

void BallPickup(int pickUpSpeed = default_pickUpSpeed);
//starts ballpickup

void BallShooter(int shooterSpeed);
//starts up (windup) shooter, loads ball, shoots, turns off motors. Time alloted for each step is configurable in common.h

void ServoGatesDefault(void);
//sets servo to default positions

void BallToLoader(void);
// Gates open to release one ball to the loader. User 
// MUST call ServoGatesDefault() couple seconds later

void LoaderDefault(void);
// Move loader servo down to the default position

void LoadBall(void);
// Loader servo goes up to load a ball to the shooter.
// MUST call LoaderDefault() after couple seconds.

boolean AligntoWall(void);
// Twerks to align to wall.
// Returns true if aligned and stopped. false if not aligned.

#endif

