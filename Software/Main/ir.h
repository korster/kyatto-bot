#ifndef IR_H
#define IR_H

#include "common.h"
#include "tapefollowing.h"  // for collision check function
#include "motorcontrol.h"  //for stopmotor()

void TestIR();

int SearchTarget();

#endif
