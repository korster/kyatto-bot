#ifndef COMMON_H
#define COMMON_H

#include <phys253.h>        //   ***** from 253 template file
#include <LiquidCrystal.h>  //   ***** from 253 template file
#include <Servo253.h>       //   ***** from 253 template file 

/******************************/
/*********TINAH PORTS**********/
/******************************/

//Digital

const int FRONT_LEFT_QRD        = 0;
const int FRONT_RIGHT_QRD	= 1;
const int ENCODER_LEFT_QRD	= 2;
const int ENCODER_RIGHT_QRD	= 3;
const int MIDDLE_LEFT_QRD 	= 4;
const int MIDDLE_RIGHT_QRD      = 5;
const int REAR_LEFT_QRD	        = 6;
const int REAR_RIGHT_QRD        = 7;

const int FRONT_LEFT_SWITCH     = 8;
const int FRONT_RIGHT_SWITCH    = 9;
const int REAR_LEFT_SWITCH      = 11;
const int REAR_RIGHT_SWITCH     = 12;

//Analog

const int  LEFT_IR             = 0;
const int  RIGHT_IR            = 1;
const int  BALL_SENSOR         = 2;

//DC Motors

const int LEFT_WHEEL	 = 0;  //up down: black red, the tinah signal always orange, grey
const int RIGHT_WHEEL	 = 1;  //up down: red black
const int PICKUP         = 2; // RED wire on TOP of half h-bridge circuit.
const int SHOOTER        = 3;
const int NUM_MOTORS     = 4;

const String motors[NUM_MOTORS] = {"L_WHEEL",
                                   "R_WHEEL",
                                   "PICKUP",
                                   "SHOOTER"
};

//ServoMotors

const int LOADER = 0;
const int SERVO2 = 1;
const int SERVO3 = 2;

const int SERVO1UP = 30;
const int SERVO2UP = 144;


/*****************************/
/***********STATES************/
/****************************/

//To add a new state, increment NUM_STATES, insert new const int STATE_NAME above NUM_STATES, and assign it one higher than the previous highest.
//Then add the name to the array, making sure that the length is 16 (shorten or add spaces if necessary)

extern int currentState;

const int TAPE_FOLLOW_FORWARDS   = 0;
const int SCAN_FOR_IR            = 1;
const int TAPE_FOLLOW_BACKWARDS  = 2;
const int PICK_UP                = 3;
const int RETURN_TO_T            = 4;
const int ALIGN_TO_WALL          = 5;
const int FULL_RUN               = 6;
const int BLIND_DRIVE_FWD        = 7;
const int DEBUG                  = 8;
const int NUM_STATES             = 9;

const String states[NUM_STATES] = {"TAPE_FLW_FWD",
                                   "SCAN_4_IR",
                                   "TAPE_FLW_BKWD",
                                   "PICK_UP",
                                   "RETURN_TO_T",
                                   "ALIGN_TO_WALL",
                                   "FULL_RUN",
                                   "DRIVE_FWD",
                                   "DEBUG"
                                   
};

const int TEST_QRDS    = 0;
const int TEST_MOTORS  = 1;
const int TEST_SERVOS  = 2;
const int TEST_TURNING = 3;
const int TEST_IR      = 4;
const int NUM_DEBUG    = 5;

const String debug[NUM_DEBUG] = {"TEST_QRDS",
                                 "TEST_MOTORS",
                                 "TEST_SERVOS",
                                 "TEST_TURNING",
                                 "TEST_IR"
};
                                 
                                 
/*****************************/
/*******TAPEFOLLOWING********/
/****************************/

static int recError;
extern int currState;
extern int wheelSpeed;
extern int upwardSpeed;
extern int downwardSpeed;
static int turnSpeed = 580;
static int scanSpeed = 600;
static int minimumSpeed = 590;

static int kp=30;
static int kd=700;
static int tapeTrack = 0;



/***************************/
/*********ENCODER***********/
/***************************/

static int adjustFactor;
static int startTime;

static int encoderRightPrev = 0;
static int encoderLeftPrev = 0;

static int angleL = 0;
static int angleR = 0;

static float slantCorrection = 0.98;
extern int encResolution;

/*****************************/
/******PICKUP & SHOOTER*******/
/*****************************/

static int pickupTime = 3000;
static int default_pickUpSpeed = 700;
static int shooterSpeed   = 190;
static int shooterStartup = 1500;
static int loaderFinish   = 2300;
static int shooterFinish  = 2800;

static int ballCount = 0;
static int failCount = 0;

/******************************/
/*************MISC************/
/****************************/

static int firstBackingUpTime = 2000;   // Tape follow backwards for this long.

const String BLANK = "                ";

#endif
