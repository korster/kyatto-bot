#ifndef UTILITIES_H
#define UTILITIES_H

#include "common.h"

namespace Utilities {
 
 String CheckStringLength(String s, int _length);
 //forces string to be a certain length by chopping off extra or adding whitespace
 
 boolean StopButton();
 //checks if stop button is pressed with a debounce time
 
 void Menu(void);
 //Enters menu:
 //top row is current, bottom row is temp(controlled by knobs), stopbutton() to set value
 //startbutton() to exit each page
}

#endif UTILITIES_H
