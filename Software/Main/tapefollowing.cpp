
#include "tapefollowing.h"
#include "utilities.h"
#include "motorcontrol.h"
#include "encoder.h"

//TapeFollowing

int currState=0;
int wheelSpeed=650;
int upwardSpeed = 680;
int downwardSpeed = -570;

void TapeFollow(int wheelSpeed, int currState) { 
  int p;
  int d;
  int con;
  int recState;
  int error=0;
  int recError = 0;
  int leftQRD = 0;
  int rightQRD = 0;
  static int lastError = 0;
  static int c;

  if (wheelSpeed > 0) {
    leftQRD = digitalRead(FRONT_LEFT_QRD);
    rightQRD = digitalRead(FRONT_RIGHT_QRD);
  }
  else if (wheelSpeed < 0) {
    leftQRD = digitalRead(REAR_RIGHT_QRD);
    rightQRD = digitalRead(REAR_LEFT_QRD);
  }

  if ((leftQRD) && (rightQRD)) {
    error = 0;
  }
  if ((leftQRD) && !(rightQRD)) {
    error = -1;
  }
  if (!(leftQRD) && (rightQRD)) {
    error = 1;
  }

  if (!(leftQRD) && !(rightQRD)) {
    if (lastError >= 0)
      error = 4;
    if (lastError < 0)
      error = -4;
  }

  if (wheelSpeed > 0 && (!(leftQRD) && !(rightQRD)))  // If tape following forward and the rear ones are on, while front ones are off the tape, go straight.
  {
    if (digitalRead(REAR_RIGHT_QRD) && digitalRead(REAR_LEFT_QRD))
      error = 0;
  }

  if (wheelSpeed == 0) {
    error = 0;
  }

  if (error != lastError)
  {
    recError = lastError;
    recState = currState;
    currState = 1;
  }

  p = kp*error;
  if (error != 0) {
    d = (int)((float)kd*(float)(error-recError)/(float)(recState + currState));
  }

  con = p+d;

  if (wheelSpeed != 0) {
    motor.speed(LEFT_WHEEL,  LimitMotorSpeed(wheelSpeed + con));
    motor.speed(RIGHT_WHEEL, LimitMotorSpeed(wheelSpeed - con));
  }
  currState++;
  //  if (wheelSpeed > 0) {
  //    motor.speed(LEFT_WHEEL,  LimitMotorSpeed(wheelSpeed + con));
  //    motor.speed(RIGHT_WHEEL, LimitMotorSpeed(wheelSpeed - con));
  //  }
  //  else if (wheelSpeed < 0){
  //    motor.speed(LEFT_WHEEL, LimitMotorSpeed(wheelSpeed + con));
  //    motor.speed(RIGHT_WHEEL, LimitMotorSpeed(wheelSpeed - con));
  //  }

  lastError = error;

  if (c==200) {
    LCD.clear();
    LCD.home();
    LCD.print("L:");
    LCD.print(leftQRD);
    LCD.print("R:"); 
    LCD.print(rightQRD);
    LCD.print("RL:"); 
    LCD.print((int) digitalRead(REAR_LEFT_QRD));
    LCD.print("RR:"); 
    LCD.print((int) digitalRead(REAR_RIGHT_QRD));
    LCD.setCursor(0,1);
    LCD.print("L:");
    LCD.print(LimitMotorSpeed(wheelSpeed + con));
    LCD.print("R:");
    LCD.print(LimitMotorSpeed(wheelSpeed - con));
    c=0;
  }
  c++;
  
  if (Utilities::StopButton() ) {
    StopMotors();
    break;
  }
  
}

boolean CheckForTapeT () {
  if ( ( (int) digitalRead(MIDDLE_LEFT_QRD) ) && ( (int) digitalRead(MIDDLE_RIGHT_QRD) ) ) {
    return true;
  }
  else {
    return false;
  }
}

boolean FrontCollision() {
  if ( !( (int) digitalRead(FRONT_LEFT_SWITCH) ) && !( (int) digitalRead(FRONT_RIGHT_SWITCH) ) ) {
    return true;
  }
  else {
    return false;
  }
}

boolean RearCollision() {
  if ( !( (int) digitalRead(REAR_LEFT_SWITCH) ) && !( (int) digitalRead(REAR_RIGHT_SWITCH) ) ) {
    return true;
  }
  else {
    return false;
  }
}

void SteerLeft() 
// This function is to be called after ball pick-up.
// This blindly steers the robot to the left, attempting to find the left tape.
// TapeFollow after this function.
{  
  double speedOffsetFactor = 1.1; // increase the outer wheel speed by this factor
  int duration = 2000; // 2 seconds 
  int delayFrontLeftQRD = 500; // 0.5 sec
  
  long int startTime = millis();
  
  // Left turn
  while( ((millis() - startTime) < duration) && !digitalRead(MIDDLE_LEFT_QRD) && !(digitalRead(FRONT_LEFT_QRD) && ((millis() - startTime) > delayFrontLeftQRD))  ) 
  { 
    /* 
    //  Left turn stops if:
    //    1. time expires
    //    2. middle QRD touches the tape
    //    3. front QRD touches the tape after some delay
    */

    motor.speed(LEFT_WHEEL,  (int)(wheelSpeed / speedOffsetFactor) );
    motor.speed(RIGHT_WHEEL, (int)(wheelSpeed * speedOffsetFactor) );
    
    static int c;
    if (c==500)
    {
    LCD.clear();
    LCD.home();
    LCD.print("L: ");
    LCD.print((int)(wheelSpeed / speedOffsetFactor));
    LCD.setCursor(0,1); LCD.print("R: ");
    LCD.print((int)(wheelSpeed * speedOffsetFactor));
    c=0;
    }
    c++;
  }
  
  // angle correction to align robot with the tape (left wheel turns more to turn right a bit)
  TurnAngle(180, 90); 
  
}

