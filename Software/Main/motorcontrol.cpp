#include "motorcontrol.h"
#include <servo253.h>       //   ***** from 253 template file 

void StopMotors() {
  int i;
  for (i=0; i<4; i++) {
    motor.speed(i, 0);
  }
}

int LimitMotorSpeed( int motorSpeed) {
  if (motorSpeed > 1023) {
    return 1023;
  } 
  else if (motorSpeed < -1023) {
    return -1023;
  } 
  else {
    return motorSpeed;
  }
}

int MinMotorSpeed(int motorSpeed){
// Minimum speed that will be able to move the robot is 'minimumSpeed'. This function returns minimumSpeed if any motorSpeed < minimumSpeed is given.
  if (motorSpeed > 0 && motorSpeed < minimumSpeed)
    return minimumSpeed;
  else if (motorSpeed > 0 && motorSpeed > minimumSpeed)
    return motorSpeed;
  else if (motorSpeed < 0 && abs(motorSpeed) > minimumSpeed)
    return motorSpeed;
  else if (motorSpeed < 0 && abs(motorSpeed) < minimumSpeed)
    return -minimumSpeed;
}

boolean BallReady(void)
{
  //Check if the ball is in the ramp.
  if (analogRead(BALL_SENSOR) < 300)
    return true;
  else
    return false;
}

void BallPickup(int pickUpSpeed) {
  motor.speed(PICKUP, LimitMotorSpeed(pickUpSpeed));
  long timeStart = millis();
  while(!BallReady() && (millis() - timeStart) < pickupTime) {
    if (! BallReady() ) {
      pickUpSpeed+=10;
    }
    motor.speed(PICKUP, LimitMotorSpeed(pickUpSpeed));
    delay(70);
    if (stopbutton() ) {
      StopMotors();
      break;
    }
  }
  delay(3000);
  StopMotors();
}

void BallShooter(int shooterSpeed) {
  long timeStart = millis();
  motor.speed(SHOOTER, shooterSpeed);
  BallToLoader();
  while(millis() <= (timeStart + shooterFinish)) {
    if (millis() >= (timeStart + shooterStartup)) {
      //Load ball after shooter has started up
      LoadBall();
    }
    if (millis() >= (timeStart + loaderFinish)) {
      //Set loader to default and stop shooter after shooting
      LoaderDefault();
      ServoGatesDefault();
      StopMotors();
    }
  }
}

/*///////////////////////////
 ////////////SERVO////////////
 ///////////////////////////*/

int up = 0;
int down = 90;

void ServoGatesDefault()
// Move servo gates to the default positions
// (Servo 1 up, Servo 2 down)
{
  RCServo1.write(0); 
  RCServo2.write(90);
}

void BallToLoader() 
// Gates open to release one ball to the loader. User 
// MUST call ServoGatesDefault() couple seconds later
{
  RCServo2.write(0);  
  delay(100);
  RCServo1.write(90);
}

void LoaderDefault()
// Move loader servo down to the default position
{
  RCServo0.write(140);
}

void LoadBall()
// Loader servo goes up to load a ball to the shooter.
// MUST call LoaderDefault() after couple seconds.
{
  RCServo0.write(30);
}

boolean AligntoWall()
/* Twerks to align to the wall.
 wheelSpeed must be defined by the PARENT function. This function will NOT change the wheelSpeed prior to execution.
 This function DOES change wheelSpeed right before the return.
 */
{
  //SPEED BOOSTER 3000 Parameters:
  long int stallStartTime;
  long int stallCurrTime;
  float correctionFactor = 0.8; // For wheel speed percentage. 0.8 Seems to work reliably.

  while (!FrontCollision() && !RearCollision())
  {
    // START MOVING!
    motor.speed(LEFT_WHEEL,wheelSpeed);
    motor.speed(RIGHT_WHEEL,wheelSpeed);

    if ( wheelSpeed > 0 && (!((int)digitalRead(FRONT_LEFT_SWITCH)) || !((int)digitalRead(FRONT_RIGHT_SWITCH))) ) {
      if (!( (int) digitalRead(FRONT_LEFT_SWITCH) ))  // If front left is hit, turn right so that right switch hits too.
      {
        stallStartTime = millis();
        while(!FrontCollision())
        {
          stallCurrTime = millis();

          if (!( (int) digitalRead(FRONT_LEFT_SWITCH) ))
          {
            motor.speed(LEFT_WHEEL, -wheelSpeed*correctionFactor);
            motor.speed(RIGHT_WHEEL, LimitMotorSpeed(wheelSpeed + (stallCurrTime-stallStartTime)/10));
          }
          // Correction for overturning.
          else if ((int) digitalRead(FRONT_LEFT_SWITCH))
          {
            motor.speed(RIGHT_WHEEL, 0);
            motor.speed(LEFT_WHEEL, LimitMotorSpeed(wheelSpeed*correctionFactor + (stallCurrTime-stallStartTime)/10));
          }
        }

        StopMotors();
        delay(1000);
        if (wheelSpeed > 0)
        {
          wheelSpeed *= -1;
        }
        // end if(!( (int) digitalRead(FRONT_LEFT_SWITCH) ))
      }
      else if (!( (int) digitalRead(FRONT_RIGHT_SWITCH) ))
      {
        stallStartTime = millis();
        while(!FrontCollision())
        {
          stallCurrTime = millis();
          if (!( (int) digitalRead(FRONT_RIGHT_SWITCH) ))
          {
            motor.speed(RIGHT_WHEEL,-wheelSpeed*correctionFactor);
            motor.speed(LEFT_WHEEL, LimitMotorSpeed(wheelSpeed + (stallCurrTime-stallStartTime)/10));
          }
          // Correction for overturning.
          else if ((int) digitalRead(FRONT_RIGHT_SWITCH))
          {
            motor.speed(LEFT_WHEEL, 0);
            motor.speed(RIGHT_WHEEL, LimitMotorSpeed(wheelSpeed*correctionFactor + (stallCurrTime-stallStartTime)/10));
          }
        }

        StopMotors();
        delay(1000);
        if (wheelSpeed > 0)
        {
          wheelSpeed *= -1;
        }
        // end if (!( (int) digitalRead(FRONT_RIGHT_SWITCH) ))
      }

      // end if ( !( (int) digitalRead(FRONT_LEFT_SWITCH) ) && !( (int) digitalRead(FRONT_RIGHT_SWITCH) ) )
    }

    if (wheelSpeed < 0 && ( !( (int) digitalRead(REAR_LEFT_SWITCH) ) || !( (int) digitalRead(REAR_RIGHT_SWITCH) ) ))
    {
      if (!( (int) digitalRead(REAR_LEFT_SWITCH) ))
      {
        stallStartTime = millis();
        while(!RearCollision())
        {
          stallCurrTime = millis();
          if (!( (int) digitalRead(REAR_LEFT_SWITCH) ))
          {
            motor.speed(RIGHT_WHEEL, -wheelSpeed*correctionFactor);
            motor.speed(LEFT_WHEEL, LimitMotorSpeed(wheelSpeed - (stallCurrTime-stallStartTime)/10));
          }
          // Correction for overturning.
          else if ((int) digitalRead(REAR_LEFT_SWITCH))
          {
            motor.speed(LEFT_WHEEL, 0);
            motor.speed(RIGHT_WHEEL, LimitMotorSpeed(wheelSpeed*correctionFactor - (stallCurrTime-stallStartTime)/10));
          }
        }
        // end if (!( (int) digitalRead(REAR_LEFT_SWITCH) ))
      }

      else if (!( (int) digitalRead(REAR_RIGHT_SWITCH) ))
      {
        stallStartTime = millis();
        while(!RearCollision())
        {
          stallCurrTime = millis();

          if (!( (int) digitalRead(REAR_RIGHT_SWITCH) ))
          {
            motor.speed(LEFT_WHEEL, -wheelSpeed*correctionFactor);
            motor.speed(RIGHT_WHEEL, LimitMotorSpeed(wheelSpeed - (stallCurrTime-stallStartTime)/10));
          }
          // Correction for overturning.
          else if ((int) digitalRead(REAR_RIGHT_SWITCH))
          {
            motor.speed(RIGHT_WHEEL, 0);
            motor.speed(LEFT_WHEEL, LimitMotorSpeed(wheelSpeed*correctionFactor - (stallCurrTime-stallStartTime)/10));
          }
        }
        // end if (!( (int) digitalRead(FRONT_RIGHT_SWITCH) ))
      }
      //    StopMotors();
      //    delay(1000);
      //    if (wheelSpeed < 0)
      //    {
      //      wheelSpeed *= -1;
      //    }
      // end if ( !( (int) digitalRead(REAR_LEFT_SWITCH) ) && !( (int) digitalRead(REAR_RIGHT_SWITCH) ) )
    }
    // end  function.
  }
  if (FrontCollision())
  {
    if(wheelSpeed > 0)
      wheelSpeed *= -1;
    return true;
  }
  if (RearCollision())
  {
    if(wheelSpeed < 0)
      wheelSpeed *= -1;
    return true;
  }
} 

